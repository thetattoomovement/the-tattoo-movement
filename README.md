The Tattoo Movement was conceptualised by Deepak Siamese VI during his tattoo journey around the world and has now found a new home in Sydney.

The Tattoo Movement has a reputation for delivering unique and custom tattoos. We promote a professional and relaxed atmosphere focusing on individual needs.

Address: Unit 21, 112 McEvoy St, Alexandria, NSW 2015, AU

Phone: +61 2 9698 0856